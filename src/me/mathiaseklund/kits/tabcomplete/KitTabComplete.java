package me.mathiaseklund.kits.tabcomplete;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import me.mathiaseklund.kits.Main;

public class KitTabComplete implements TabCompleter {

	Main main = Main.getMain();

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<String>();
		if (sender instanceof Player) {
			Player player = (Player) sender;
			List<String> pot = new ArrayList<String>();
			if (args.length == 1) {
				if (player.hasPermission("kit.create")) {
					pot.add("create");
				}
				for (String s : main.getKitManager().getAllKitNames()) {
					pot.add(s);
				}

				for (String s : pot) {
					if (s.contains(args[0])) {
						list.add(s);
					}
				}
			}
		}
		return list;
	}

}
