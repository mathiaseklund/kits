package me.mathiaseklund.kits;

import java.io.File;
import java.sql.SQLException;

import org.bukkit.plugin.java.JavaPlugin;

import me.mathiaseklund.kits.commands.KitCommand;
import me.mathiaseklund.kits.events.Events;
import me.mathiaseklund.kits.managers.KitManager;
import me.mathiaseklund.kits.managers.SQLManager;
import me.mathiaseklund.kits.tabcomplete.KitTabComplete;
import me.mathiaseklund.kits.utils.Util;

public class Main extends JavaPlugin {

	static Main main;

	// HikariDataSource hikari;
	SQLManager sql;
	KitManager kit;
	Util util;

	public static Main getMain() {
		return main;
	}

	public void onEnable() {
		main = this;

		loadFiles();
		loadManagers();
		loadCommands();
		loadEvents();

	}

	public void onDisable() {
		// if (sql.getHikari().isClosed()) {
		// sql.getHikari().close();
		// }
		try {
			if (sql.getConnection() != null) {
				if (!sql.getConnection().isClosed()) {
					sql.getConnection().close();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void loadFiles() {
		File f = new File(getDataFolder(), "config.yml");
		if (!f.exists()) {
			saveResource("config.yml", true);
		}

		f = new File(getDataFolder() + "/kits/");
		if (!f.exists()) {
			f.mkdir();
		}
	}

	void loadManagers() {
		util = new Util();
		sql = new SQLManager();
		kit = new KitManager();
	}

	void loadCommands() {
		getCommand("kit").setExecutor(new KitCommand());
		getCommand("kit").setTabCompleter(new KitTabComplete());
	}

	void loadEvents() {
		getServer().getPluginManager().registerEvents(new Events(), this);
	}

	public SQLManager getSQLManager() {
		return sql;
	}

	public Util getUtil() {
		return util;
	}

	public KitManager getKitManager() {
		return kit;
	}

}
