package me.mathiaseklund.kits.managers;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.kits.Main;
import me.mathiaseklund.kits.utils.Util;

public class KitManager {

	Main main;
	Util util;

	File folder;
	HashMap<String, File> files;
	HashMap<String, FileConfiguration> fileConfigs;
	HashMap<String, HashMap<String, Long>> kitCooldowns = new HashMap<String, HashMap<String, Long>>();

	public KitManager() {
		main = Main.getMain();
		util = main.getUtil();

		load();
	}

	void load() {

		files = new HashMap<String, File>();
		fileConfigs = new HashMap<String, FileConfiguration>();

		File f = new File(main.getDataFolder() + "/kits/");
		if (f.exists()) {
			File[] list = f.listFiles();
			for (File file : list) {
				String kitName = file.getName().replace(".yml", "");
				FileConfiguration fc = YamlConfiguration.loadConfiguration(file);
				files.put(kitName, file);
				fileConfigs.put(kitName, fc);
			}
			System.out.println("Loaded " + list.length + " kits.");
		} else {
			util.error(Bukkit.getConsoleSender(), "Unable to find kits folder.");
		}
	}

	public void loadPlayerData(Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				HashMap<String, Long> cds = new HashMap<String, Long>();
				Connection conn = main.getSQLManager().getConnection();
				try {
					PreparedStatement select = conn.prepareStatement("SELECT kit, time FROM kitusage WHERE uuid=?");
					select.setString(1, player.getUniqueId().toString());
					ResultSet result = select.executeQuery();
					while (result.next()) {
						String kitName = result.getString("kit");
						long time = result.getLong("time");
						cds.put(kitName, time);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}

				kitCooldowns.put(player.getUniqueId().toString(), cds);
			}
		});
	}

	/*
	 * In case kit content should replace inventory and have set slots.
	 * 
	 * public HashMap<Integer, ItemStack> getKitContent(String kitName) {
	 * HashMap<Integer, ItemStack> map = new HashMap<Integer, ItemStack>();
	 * FileConfiguration fc = fileConfigs.get(kitName); Set<String> keys =
	 * fc.createSection("content").getKeys(false); for (String key : keys) { int
	 * slot = Integer.parseInt(key); ItemStack is = fc.getItemStack("content." +
	 * key); map.put(slot, is); } return map; }
	 */

	/*
	 * in case armor should be insta equipped.
	 * 
	 * public HashMap<EquipmentSlot, ItemStack> getKitArmor(String kitName) {
	 * HashMap<EquipmentSlot, ItemStack> map = new HashMap<EquipmentSlot,
	 * ItemStack>(); FileConfiguration fc = fileConfigs.get(kitName); Set<String>
	 * keys = fc.createSection("content").getKeys(false); for (String key : keys) {
	 * EquipmentSlot slot = EquipmentSlot.valueOf(key); ItemStack is =
	 * fc.getItemStack("armor." + key); map.put(slot, is); } return map; }
	 */

	public List<ItemStack> getKitContent(String kitName) {
		List<ItemStack> list = new ArrayList<ItemStack>();
		FileConfiguration fc = fileConfigs.get(kitName);
		Set<String> keys = fc.getConfigurationSection("content").getKeys(false);
		for (String key : keys) {
			ItemStack is = fc.getItemStack("content." + key);
			list.add(is);
		}
		return list;
	}

	public void createKit(Player player, String kitName, int delay) {
		if (player != null) {
			if (kitName != null) {
				if (delay > 0) {
					List<ItemStack> list = new ArrayList<ItemStack>();
					for (ItemStack is : player.getInventory().getContents()) {
						if (is != null && is.getType() != Material.AIR) {
							list.add(is);
						}
					}

					if (!list.isEmpty()) {

						File file = new File(main.getDataFolder() + "/kits/" + kitName + ".yml");
						try {
							file.createNewFile();
							FileConfiguration fc = YamlConfiguration.loadConfiguration(file);

							long d = delay * 60 * 1000;
							fc.set("delay", d);
							for (int i = 0; i < list.size(); i++) {
								fc.set("content." + i, list.get(i));
							}
							fc.save(file);

							files.put(kitName, file);
							fileConfigs.put(kitName, fc);

							util.message(player, "&bNew kit created called: &e" + kitName);
						} catch (IOException e) {
							util.error(player, "Unable to create the kit because of file storage problems.");
							e.printStackTrace();
						}

					} else {
						util.error(player, "Kit content can't be empty. Put some items in your inventory.");
					}

				} else {
					util.error(player, "Kit needs to have a delay at or above 1 Minute(s).");
				}
			} else {
				util.error(player, "Kit needs to have a name.");
			}
		} else {
			// erm, shouldn't ever happen.
		}
	}

	public boolean kitExists(String kitName) {
		return files.containsKey(kitName);
	}

	public void attemptKitClaim(Player player, String kitName) {

		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (kitExists(kitName)) {
					if (canClaimKit(player, kitName)) {
						long delay = getKitDelay(kitName);
						long now = Calendar.getInstance().getTimeInMillis();

						HashMap<String, Long> cds = kitCooldowns.get(player.getUniqueId().toString());
						cds.put(kitName, now + delay);
						kitCooldowns.put(player.getUniqueId().toString(), cds);

						for (ItemStack is : getKitContent(kitName)) {
							int empty = player.getInventory().firstEmpty();
							if (empty >= 0) {
								Bukkit.getScheduler().runTask(main, new Runnable() {
									public void run() {
										player.getInventory().addItem(is);
									}
								});
							} else {
								Bukkit.getScheduler().runTask(main, new Runnable() {
									public void run() {
										player.getWorld().dropItem(player.getLocation(), is);
									}
								});

							}
						}

						util.message(player,
								main.getConfig().getString("message.claimed-kit", "&eYou've claimed the kit: %kit%")
										.replace("%kit%", kitName));

						Connection conn = main.getSQLManager().getConnection();
						try {
							PreparedStatement sel = conn
									.prepareStatement("SELECT time FROM kitusage WHERE kit=? AND uuid=? LIMIT 1");
							sel.setString(1, kitName);
							sel.setString(2, player.getUniqueId().toString());

							ResultSet res = sel.executeQuery();
							if (res.next()) {
								PreparedStatement update = conn
										.prepareStatement("UPDATE kitusage SET time=? WHERE kit=? AND uuid=?");
								update.setLong(1, now + delay);
								update.setString(2, kitName);
								update.setString(3, player.getUniqueId().toString());
								update.execute();
								update.close();
							} else {
								PreparedStatement insert = conn
										.prepareStatement("INSERT INTO kitusage (kit, uuid, time) VALUES(?, ?, ?)");
								insert.setString(1, kitName);
								insert.setString(2, player.getUniqueId().toString());
								insert.setLong(3, now + delay);
								insert.execute();
								insert.close();
							}
							res.close();
							sel.close();

						} catch (SQLException e) {
							e.printStackTrace();
						}

					} else {
						// util.error(player, "You are unable to claim this kit.");
					}
				} else {
					util.error(player, "Requested kit does not exist.");
				}
			}
		});
	}

	public boolean canClaimKit(Player player, String kitName) {
		boolean b = false;
		if (!player.hasPermission("kits.exempt")) {
			long now = Calendar.getInstance().getTimeInMillis();
			if (kitCooldowns.containsKey(player.getUniqueId().toString())) {
				HashMap<String, Long> cds = kitCooldowns.get(player.getUniqueId().toString());
				if (cds.containsKey(kitName)) {
					long time = cds.get(kitName);
					if (time <= now) {
						b = true;
					} else {
						long diff = time - now;

						String tString = main.getConfig().getString("message.cooldown",
								"&cYou can't use this kit for %days%%hours%%minutes%%seconds%!");
						long seconds = diff / 1000;
						long minutes = seconds / 60;
						long hours = minutes / 60;
						long days = hours / 24;
						String dString = days % 24 + " Days ";
						String hString = hours % 24 + " Hours ";
						String mString = minutes % 60 + " Minutes ";
						String sString = seconds % 60 + " Seconds";

						if (days > 0) {
							tString = tString.replace("%days%", dString);
						} else {
							tString = tString.replace("%days%", "");
						}

						if (hours > 0) {
							tString = tString.replace("%hours%", hString);
						} else {
							tString = tString.replace("%hours%", "");
						}

						if (minutes > 0) {
							tString = tString.replace("%minutes%", mString);
						} else {
							tString = tString.replace("%minutes%", "");
						}

						tString = tString.replace("%seconds%", sString);

						util.message(player, tString);

					}
				} else {
					b = true;
				}
			} else {
				b = true;
			}
		} else {
			b = true;
		}
		return b;

	}

	public long getKitDelay(String kitName) {
		FileConfiguration fc = fileConfigs.get(kitName);
		return fc.getLong("delay");
	}

	public Set<String> getAllKitNames() {
		return files.keySet();
	}
}
