package me.mathiaseklund.kits.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import me.mathiaseklund.kits.Main;
import me.mathiaseklund.kits.managers.KitManager;
import me.mathiaseklund.kits.utils.Util;

public class Events implements Listener {

	Main main = Main.getMain();
	Util util = main.getUtil();
	KitManager manager = main.getKitManager();

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerLogin(PlayerLoginEvent event) {
		if (event.getResult() == Result.ALLOWED) {
			manager.loadPlayerData(event.getPlayer());
		}
	}
}
