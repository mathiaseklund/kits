package me.mathiaseklund.kits.utils;

import org.bukkit.command.CommandSender;

import me.mathiaseklund.kits.Main;
import net.md_5.bungee.api.ChatColor;

public class Util {

	Main main;

	public Util() {
		main = Main.getMain();
	}

	public void error(CommandSender sender, String message) {
		if (sender != null && message != null) {
			String msg = main.getConfig().getString("message.error-format", "&4[ERROR]&7 %message%");
			msg = msg.replace("%message%", message);
			message(sender, msg);
		}

	}

	public void message(CommandSender sender, String message) {
		if (sender != null && message != null) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
		}
	}

	public boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
}
