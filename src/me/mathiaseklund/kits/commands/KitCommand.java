package me.mathiaseklund.kits.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.kits.Main;
import me.mathiaseklund.kits.utils.Util;

public class KitCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					if (args.length == 0) {
						if (player.hasPermission("kit.create")) {
							util.message(sender,
									"&e/kit create <name> <delay> &f- Example: /kit create example 10 for a kit with a 10 minute delay.");
						}
						util.message(sender, "&e/kit <name>");
					} else {
						if (args[0].equalsIgnoreCase("create")) {
							if (sender.hasPermission("kit.create")) {
								int delay = 0;
								String kitName = null;
								if (args.length == 3) {
									kitName = args[1].toLowerCase();
									try {
										delay = Integer.parseInt(args[2]);

										main.getKitManager().createKit(player, kitName, delay);
									} catch (NumberFormatException e) {
										util.error(player, "Delay argument must be an Integer.");
									}
								}
							} else {
								util.error(sender, "You do not have permission to use this command.");
							}
						} else {
							// TODO claim a kit.
							String kitName = args[0].toLowerCase();
							if (player.hasPermission("kit.use." + kitName)) {
								main.getKitManager().attemptKitClaim(player, kitName);
							} else {
								util.error(sender, "You do not have permission to claim this kit.");
							}
						}
					}
				} else {
					util.error(sender, "This command can only be run by a player.");
				}
			}
		});
		return false;
	}

}
