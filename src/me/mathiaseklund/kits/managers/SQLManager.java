package me.mathiaseklund.kits.managers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mathiaseklund.kits.Main;
import me.mathiaseklund.kits.utils.Util;

public class SQLManager {

	Main main;
	Util util;

	// HikariDataSource hikari;

	Connection connection;
	String host, user, password, database;
	int port;

	public SQLManager() {
		main = Main.getMain();
		util = main.getUtil();

		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				setupDatabase();
			}
		});

	}

	/*
	 * Connect to the database.
	 */
	void setupDatabase() { // Connect to the database.
		host = main.getConfig().getString("mysql.host", "localhost");
		port = main.getConfig().getInt("mysql.port", 3306);
		database = main.getConfig().getString("mysql.database", "kits");
		user = main.getConfig().getString("mysql.user", "dev");
		password = main.getConfig().getString("mysql.password", "password");

		try {
			openConnection();
			createTables();
			loadOnlinePlayers();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// hikari = new HikariDataSource();
		// hikari.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
		// hikari.addDataSourceProperty("serverName", host);
		// hikari.addDataSourceProperty("port", port);
		// hikari.addDataSourceProperty("databaseName", database);
		// hikari.addDataSourceProperty("user", user);
		// hikari.addDataSourceProperty("password", password);

		createTables();
	}

	/*
	 * Setup the required database tables if they don't exist.
	 */
	void createTables() {
		try {
			Connection conn = getConnection();
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS kitusage (kit VARCHAR(36), uuid VARCHAR(36), time BIGINT)");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Get the hikari data source object.
	 */
	// public HikariDataSource getHikari() {
	// return hikari;
	// }

	void openConnection() throws SQLException, ClassNotFoundException {
		if (connection != null && !connection.isClosed()) {

		}

		synchronized (this) {
			if (connection != null && !connection.isClosed()) {
				return;
			}
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(
					"jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database, this.user, this.password);
		}
	}

	public Connection getConnection() {
		try {
			openConnection();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}

	/**
	 * Load the cooldown of any players already online on plugin load.
	 */
	void loadOnlinePlayers() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			main.getKitManager().loadPlayerData(p);
		}
	}
}
